# Ingress FastRoute

Is nodejs package that give Ingress Prime players recomendation route to play the game.

## Installation

Use the package manager 

### Yarn

[yarn](https://yarnpkg.com/cli/install) to install Ingress FastRoute.

```bash
yarn install
```

## Usage

```js
const fastRoute = require('ingress-fastroute');
fastRoute.calc('lat, lng', 'lat, lng')
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)